package com.example.celiko.model;

/**
 * Created by celiko on 2015-07-13.
 */

public class Contractor {
    private int idContractor;
    private String city;
    private String fax;
    private String name;
    private String nip;
    private String postcode;
    private String regon;
    private String street;
    private String telNumber;
    private String email;
    private int idUser;
    public Contractor(){

    }
    public Contractor(String city, String fax, String name, String nip, String postcode, String regon, String street, String telNumber, String email, int idUser) {
        this.city = city;
        this.fax = fax;
        this.name = name;
        this.nip = nip;
        this.postcode = postcode;
        this.regon = regon;
        this.street = street;
        this.telNumber = telNumber;
        this.email = email;
        this.idUser = idUser;
    }



    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdContractor() {
        return idContractor;
    }

    public void setIdContractor(int idContractor) {
        this.idContractor = idContractor;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getRegon() {
        return regon;
    }

    public void setRegon(String regon) {
        this.regon = regon;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
