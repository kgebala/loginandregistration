package com.example.celiko.model;

/**
 * Created by celiko on 2015-07-13.
 */

public class SharedContractor {
    private int isSharedContractorId;
    private int isShared;
    private int contractor_idContractor;
    private int user_idUser;

    public int getIsSharedContractorId() {
        return isSharedContractorId;
    }

    public void setIsSharedContractorId(int isSharedContractorId) {
        this.isSharedContractorId = isSharedContractorId;
    }

    public int getIsShared() {
        return isShared;
    }

    public void setIsShared(int isShared) {
        this.isShared = isShared;
    }

    public int getContractor_idContractor() {
        return contractor_idContractor;
    }

    public void setContractor_idContractor(int contractor_idContractor) {
        this.contractor_idContractor = contractor_idContractor;
    }

    public int getUser_idUser() {
        return user_idUser;
    }

    public void setUser_idUser(int user_idUser) {
        this.user_idUser = user_idUser;
    }
}
