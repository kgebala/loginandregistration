package com.example.celiko.loginandregistration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.celiko.helper.SQLiteHandler;
import com.example.celiko.helper.SessionManager;
import com.example.celiko.model.Contractor;
import com.example.celiko.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ContractorActivity extends AppCompatActivity {
    private static final String TAG = ContractorActivity.class.getSimpleName();

    private EditText simpleName;
    private EditText email;
    private EditText city;
    private EditText street;
    private EditText postcode;
    private EditText telNumber;
    private EditText fax;
    private EditText nip;
    private EditText regon;
    private Button btnAddContractor;

    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contractor);

        simpleName = (EditText)findViewById(R.id.simpleName);
        email = (EditText) findViewById(R.id.email);
        city = (EditText) findViewById(R.id.city);
        street = (EditText) findViewById(R.id.street);
        postcode = (EditText) findViewById(R.id.postcode);
        telNumber = (EditText) findViewById(R.id.telNumber);
        fax = (EditText) findViewById(R.id.fax);
        nip = (EditText) findViewById(R.id.nip);
        regon = (EditText) findViewById(R.id.regon);
        btnAddContractor = (Button) findViewById(R.id.btnAddContractor);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());
        // Check if user is already logged in or not
//        if (session.isLoggedIn()) {
//            // User is already logged in. Take him to main activity
//            Intent intent = new Intent(ContractorActivity.this,
//                    MainActivity.class);
//            startActivity(intent);
//            finish();
//        }

        btnAddContractor.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
               Contractor contractor= new Contractor();

                contractor.setName(simpleName.getText().toString());
                contractor.setEmail(email.getText().toString());
                contractor.setCity(city.getText().toString());
                contractor.setStreet(street.getText().toString());
                contractor.setPostcode(postcode.getText().toString());
                contractor.setTelNumber(telNumber.getText().toString());
                contractor.setFax(fax.getText().toString());
                contractor.setNip(nip.getText().toString());
                contractor.setRegon(regon.getText().toString());

                if(!session.isLoggedIn().equals("logout")){
                    //getuserdetails to mapa stąd get bo pobieramy value
                    String idtmp=db.getUserDetails(session.isLoggedIn()).get("idUser");


                    contractor.setIdUser(Integer.parseInt(idtmp));
                } if (
                        !contractor.getName().isEmpty() &&
                        !contractor.getEmail().isEmpty() &&
                        !contractor.getCity().isEmpty() &&
                        !contractor.getStreet().isEmpty() &&
                        !contractor.getPostcode().isEmpty() &&
                        !contractor.getTelNumber().isEmpty() &&
                        !contractor.getFax().isEmpty() &&
                        !contractor.getNip().isEmpty() &&
                        !contractor.getRegon().isEmpty()
                        ) {
                    registerContractor(contractor);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }
    private void registerContractor(final Contractor c) {
        // Tag used to cancel the request
        String tag_string_req = "req_register";
         pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTER,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "Register Response: " + response.toString());
                        hideDialog();
// tymczasowe rozwiązanie

                        String tmpId= db.getUserDetails(session.isLoggedIn()).get("idUser");
                        try {
                            JSONObject jObj = new JSONObject(response);
                            boolean error = jObj.getBoolean("error");
                            if (!error) {
                                // User successfully stored in MySQL
                                // Now store the user in sqlite
                                JSONObject jsonUser = jObj.getJSONObject("contractor");
                                c.setName(jsonUser.getString("name"));
                              //  c.setEmail(jsonUser.getString("email"));
                                c.setCity(jsonUser.getString("city"));
                                c.setStreet(jsonUser.getString("street"));
                                c.setPostcode(jsonUser.getString("postcode"));
                                c.setTelNumber(jsonUser.getString("telNumber"));
                                c.setFax(jsonUser.getString("fax"));
                                c.setNip(jsonUser.getString("nip"));
                                c.setRegon(jsonUser.getString("regon"));
                                //to tez tak nie zostanie
                                c.setIdUser(Integer.parseInt(jsonUser.getString("idUser")));
                                // Inserting row in users table
                                db.addContractor(c);

                                // Launch login activity
                                Intent intent = new Intent(
                                        ContractorActivity.this,
                                        MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else {

                                // Error occurred in registration. Get the error
                                // message
                                String errorMsg = jObj.getString("error_msg");
                                Toast.makeText(getApplicationContext(),
                                        errorMsg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
//                 Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("tag", "addContractor");
                params.put("name", c.getName());
                //params.put("Email", c.getEmail());
                params.put("city", c.getCity());
                params.put("street", c.getStreet());
                params.put("postcode", c.getPostcode());
                params.put("telNumber", c.getTelNumber());
                params.put("fax", c.getFax());
                params.put("nip", c.getNip());
                params.put("regon", c.getRegon());
                params.put("idUser", String.valueOf(c.getIdUser()));
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contractor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
