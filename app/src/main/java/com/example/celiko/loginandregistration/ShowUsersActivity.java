package com.example.celiko.loginandregistration;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.celiko.adapter.UserAdapter;
import com.example.celiko.helper.SQLiteHandler;
import com.example.celiko.model.User;

import java.util.List;

public class ShowUsersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_users);
        populateUsers();
    }

    private void populateUsers() {
        SQLiteHandler dm = new SQLiteHandler(this);
        List<User> allUsers = dm.getAll();

        UserAdapter adapter = new UserAdapter(this, R.layout.users_list, allUsers );
        ListView listView = (ListView) findViewById(R.id.contractorListView);
        listView.setAdapter(adapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_users, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
