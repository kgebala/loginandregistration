package com.example.celiko.loginandregistration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.celiko.adapter.ContractorAdapter;
import com.example.celiko.helper.SQLiteHandler;
import com.example.celiko.helper.SessionManager;
import com.example.celiko.model.Contractor;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {
    public final static String EXTRA_MESSAGE = "com.example.celiko.MESSAGE";

    private TextView txtFirstName;
    private TextView txtLastName;
    private TextView txtLogin;
    private TextView txtEmail;
    private Button btnAddContractor;
    private Button btnLogout;

    private SQLiteHandler db;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtFirstName = (TextView) findViewById(R.id.firstName);
        txtLastName = (TextView) findViewById(R.id.lastName);
        txtLogin = (TextView) findViewById(R.id.login);
        txtEmail = (TextView) findViewById(R.id.email);
        btnAddContractor = (Button)findViewById(R.id.btnAddContractor);
        btnLogout = (Button) findViewById(R.id.btnLogout);

        db = new SQLiteHandler(getApplicationContext());

        session = new SessionManager(getApplicationContext());

        if (session.isLoggedIn().equals("logout")) {
                logoutUser();
        }
            //pobieranie wiersza
            HashMap<String, String> user = db.getUserDetails(session.isLoggedIn());

            String firstName = user.get("firstName");
            String lastName = user.get("lastName");
            String email = user.get("email");
            String login = user.get("login");
            //to jest rozwiązanie doraźne
            int userId=0;
            if(user.size()!=0){
                userId = Integer.parseInt(user.get("idUser"));
            }
                //wyswietlanie danych
            txtFirstName.setText(firstName);
            txtLastName.setText(lastName);
            txtEmail.setText(email);
            txtLogin.setText(login);
            populateContractors(userId);




        btnAddContractor.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        ContractorActivity.class);
                startActivity(i);
                finish();
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
        //click listener na elemencie listy
        registerOnClickCallback();

    }

    private void populateContractors(int idUser) {
        SQLiteHandler dm = new SQLiteHandler(this);
        List<Contractor> allContractors = dm.getAllContractors(idUser);

        ContractorAdapter adapter = new ContractorAdapter(this, R.layout.users_list, allContractors );
        ListView listView = (ListView) findViewById(R.id.contractorListView);
        listView.setAdapter(adapter);
    }

    private void registerOnClickCallback() {
        ListView listView = (ListView) findViewById(R.id.contractorListView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(MainActivity.this, ContractorManager.class);
                Integer message = position + 1;
                intent.putExtra(EXTRA_MESSAGE, message.toString());
                startActivity(intent);
            }
        });

    }

    private void logoutUser() {
        session.setLogin("logout");

        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    //db.deleteUsers();


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
