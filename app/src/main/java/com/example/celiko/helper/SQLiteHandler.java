package com.example.celiko.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.celiko.model.Contractor;
import com.example.celiko.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by celiko on 2015-06-30.
 */
public class SQLiteHandler extends SQLiteOpenHelper {
    public static final String TAG = SQLiteHandler.class.getSimpleName();
    //wersja bazy danych
    public static final int DATABASE_VERSION = 1;
    //nazwa bazy danych
    public static final String DATABASE_NAME = "android_api";
    //tabela login
    public static final String TABLE_LOGIN = "login";
    //tabela contractor
    public static final String TABLE_CONTRACTOR = "contractor";
    //tabela sharedcontractor
    public static final String TABLE_SHAREDCONTRACTOR = "sharedcontractor";
    //nazwy kolumn tabeli login
    public static final String KEY_ID = "id";
    public static final String KEY_FIRST_NAME = "firstName";
    public static final String KEY_LAST_NAME = "lastName";
    public static final String KEY_LOGIN = "login";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_IDUSER = "idUser";
    public static final String KEY_CERATED_AT = "created_at";
    //nazwy kolumn tabeli contractor
    public static final String KEY_CONTRACTOR_ID = "idContractor";
    public static final String KEY_CITY = "city";
    public static final String KEY_FAX = "fax";
    public static final String KEY_NAME = "name";
    public static final String KEY_NIP = "nip";
    public static final String KEY_POSTCODE = "postcode";
    public static final String KEY_REGON = "regon";
    public static final String KEY_STREET = "street";
    public static final String KEY_TELNUMBER = "telNumber";
    //nazwy kolumn tabeli sharedcontractor
    public static final String KEY_IS_SHAREDCONTRACTOR_ID = "isSharedContractorId";
    public static final String KEY_IS_SHARED = "isShared";
    public static final String KEY_CONTRACTOR_CONTRACTOR_ID= "contractor_idContractor";
    public static final String KEY_USER_USER_ID = "user_idUser";
    public SQLiteHandler(Context context) {
        //null to SQLiteDatabase.CursorFactory, cursor służy do operacji na bazie
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_LOGIN + "("
                + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_FIRST_NAME + " TEXT, "
                + KEY_LAST_NAME + " TEXT, "
                + KEY_EMAIL + " TEXT UNIQUE,"
                + KEY_LOGIN + " TEXT, "
                + KEY_IDUSER + " TEXT, "
                + KEY_CERATED_AT + " TEXT " + ")";
        //execSQL() wykonuje pojedyńczą instrukcję, która nie zwraca wyniku
        String CREATE_CONTRACTOR_TABLE = "CREATE TABLE " + TABLE_CONTRACTOR + "("
                + KEY_CONTRACTOR_ID + " INTEGER PRIMARY KEY, "
                + KEY_CITY+ " TEXT, "
                + KEY_FAX + " TEXT, "
                + KEY_NAME + " TEXT, "
                + KEY_NIP + " TEXT, "
                + KEY_POSTCODE + " TEXT,"
                + KEY_REGON + " TEXT, "
                + KEY_STREET + " TEXT, "
                + KEY_TELNUMBER + " TEXT, "
                + KEY_IDUSER + " INTEGER, "
                + "FOREIGN KEY (" + KEY_IDUSER
                + ") REFERENCES " + TABLE_LOGIN + " ("+ KEY_ID +")"
                + ")";

        String CREATE_SHAREDCONTRACTOR_TABLE = "CREATE TABLE " + TABLE_SHAREDCONTRACTOR + "("
                + KEY_IS_SHAREDCONTRACTOR_ID + " INTEGER PRIMARY KEY, "
                + KEY_IS_SHARED + " INTEGER, "
                + KEY_CONTRACTOR_CONTRACTOR_ID + " INTEGER, "
                + KEY_USER_USER_ID + " INTEGER "
                + "FOREIGN KEY (" + KEY_CONTRACTOR_CONTRACTOR_ID +
                ") REFERENCES " + TABLE_CONTRACTOR + " ("+ KEY_CONTRACTOR_ID +"), "
                + "FOREIGN KEY (" + KEY_USER_USER_ID +
                ") REFERENCES " + TABLE_LOGIN + " ("+ KEY_ID +")"
                + ")";

        db.execSQL(CREATE_LOGIN_TABLE);
        db.execSQL(CREATE_CONTRACTOR_TABLE);
        db.execSQL(CREATE_SHAREDCONTRACTOR_TABLE);
        Log.d(TAG, "Database tables created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABLE_LOGIN);
        //utworzenie bazy ponownie, przez onCreate
        onCreate(db);
    }

    public void addUser(User u) {
        //baza danych w trybie zapisu
        SQLiteDatabase db = this.getWritableDatabase();
        //klasa uzywana do przechowywania vartosci zapusywanych do bazy
        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_NAME, u.getFirstName());
        values.put(KEY_LAST_NAME, u.getLastName());
        values.put(KEY_EMAIL, u.getEmail());
        values.put(KEY_LOGIN, u.getLogin());
        values.put(KEY_IDUSER, u.getExternalId());
        values.put(KEY_CERATED_AT, u.getCreatedAt());
        long id = db.insert(TABLE_LOGIN, null, values);
        db.close();
        Log.d(TAG, "New user inserted into sqlite: " + u.getExternalId());
    }


    public HashMap<String, String> getUserDetails(String login) {
        HashMap<String, String> user = new HashMap<>();
        String selectQuery = "SELECT * FROM " + TABLE_LOGIN+" WHERE login= ?";
        SQLiteDatabase db = this.getReadableDatabase();
        //rawQuery() zwraca wyniki zapytania(zbiór wyników) w postaci Cursora
        Cursor cursor;
        if(login!=null) {
            cursor = db.rawQuery(selectQuery, new String[]{login});
        }else{
            cursor = db.rawQuery(selectQuery, null);
        }
        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            user.put("firstName", cursor.getString(1));
            user.put("lastName", cursor.getString(2));
            user.put("email", cursor.getString(3));
            user.put("login", cursor.getString(4));
            user.put("idUser", cursor.getString(5));
            user.put("created_at", cursor.getString(6));
        }
        cursor.close();
        db.close();
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());
        return user;
    }

    public int getRowCount() {
        String countQuery = "SELECT * FROM " + TABLE_LOGIN;
        SQLiteDatabase db= this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        db.close();
        cursor.close();

        return rowCount;
    }

    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // usuwa wszystkie wiersze
        db.delete(TABLE_LOGIN, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }
    public List<User> getAll() {
        List<User> allUsers = new ArrayList<>();

        String[] columns = {KEY_ID, KEY_IDUSER, KEY_FIRST_NAME, KEY_LAST_NAME, KEY_LOGIN, KEY_CERATED_AT };
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_LOGIN, columns, null, null, null, null, null);
        while (cursor.moveToNext()) {
            User u = new User();
            u.setId(cursor.getInt(0));
            u.setExternalId(cursor.getString(1));
            u.setFirstName(cursor.getString(2));
            u.setLastName(cursor.getString(3));
            u.setLogin(cursor.getString(4));
            u.setCreatedAt(cursor.getString(5));
            allUsers.add(u);
        }
        return allUsers;
    }
    ///contractor functions!!!!!!!!
    public void addContractor(Contractor c){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
//
        cv.put(KEY_CITY, c.getCity());
        cv.put(KEY_FAX, c.getFax());
        cv.put(KEY_NAME, c.getName());
        cv.put(KEY_NIP, c.getNip());
        cv.put(KEY_POSTCODE, c.getPostcode());
        cv.put(KEY_REGON, c.getRegon());
        cv.put(KEY_STREET, c.getStreet());
        cv.put(KEY_TELNUMBER, c.getTelNumber());
        cv.put(KEY_IDUSER, c.getIdUser());

        db.insertOrThrow(TABLE_CONTRACTOR, null, cv);
    }

    public List<Contractor> getAllContractors(int userId) {
        List<Contractor> allContractors = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = {KEY_CONTRACTOR_ID, KEY_CITY, KEY_FAX, KEY_NAME, KEY_NIP, KEY_POSTCODE,
                KEY_REGON, KEY_STREET, KEY_TELNUMBER, KEY_IDUSER};

        String[] args = {userId + ""};
        Cursor cursor = db.query(TABLE_CONTRACTOR, columns, KEY_IDUSER+"=?", args, null, null, null);

        while (cursor.moveToNext()) {
            Contractor c = new Contractor();
            c.setIdContractor(cursor.getInt(0));
            c.setCity(cursor.getString(1));
            c.setFax(cursor.getString(2));
            c.setName(cursor.getString(3));
            c.setNip(cursor.getString(4));
            c.setPostcode(cursor.getString(5));
            c.setRegon(cursor.getString(6));
            c.setStreet(cursor.getString(7));
            c.setTelNumber(cursor.getString(8));
            c.setIdUser(cursor.getInt(9));
            allContractors.add(c);
        }
        return allContractors;
    }
    public void deleteContractor(int id) {
        SQLiteDatabase db = getWritableDatabase();
        String[] arguments = {" " +id};
        db.delete(TABLE_CONTRACTOR, KEY_CONTRACTOR_ID + "=?", arguments);
    }
}
