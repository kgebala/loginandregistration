package com.example.celiko.helper;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

/**
 * Created by celiko on 2015-06-30.
 */
public class SessionManager {
    private static String TAG = SessionManager.class.getSimpleName();
    //w wielkim skrócie pozwala przechowywać dane po zamknięciu aplikacji
    SharedPreferences pref;
    // interfejs modyfikujący wartości sharedPreferences
    Editor editor;
    Context _context;

    int PRIVATE_MODE = 0;
    //nazwa pliku
    private static final String PREF_NAME = "SharedPrefName";
    private static  final String KEY_IS_LOGGEDIN = "isLoggedIn";

    public SessionManager (Context context){
        this._context=context;
        pref= _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor= pref.edit();
    }
    public void setLogin(String isLoggedIn){
        editor.putString(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.commit();
        //potestować rozne logi
        Log.d(TAG, "User login session modified!");
    }
    //zwraca preferencje, gdy jest pusta zwraca drugi parametr
    public String isLoggedIn(){
        return pref.getString(KEY_IS_LOGGEDIN, "logout");
    }
}
