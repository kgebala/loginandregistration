package com.example.celiko.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.celiko.loginandregistration.R;
import com.example.celiko.model.Contractor;


import java.util.List;

/**
 * Created by celiko on 2015-07-13.
 */
public class ContractorAdapter extends ArrayAdapter<Contractor> {
    Context context;
    int resource;
    List<Contractor> contractorList=null;
    public ContractorAdapter(Context context, int resource, List<Contractor> contractorList) {
        super(context, resource, contractorList);
        this.context = context;
        this.resource = resource;
        this.contractorList = contractorList;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        RowBeanHolder holder = null;
        if(itemView== null){

            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            itemView = inflater.inflate(resource, parent, false);
            holder = new RowBeanHolder();
            holder.txtTitle=(TextView)itemView.findViewById(R.id.textView1);

            itemView.setTag(holder);
        }else {
            holder=(RowBeanHolder) itemView.getTag();
        }

        Contractor c = contractorList.get(position);
        holder.txtTitle.setText(c.getIdUser()+" "+c.getIdContractor()+" "+ c.getName()+" "+c.getCity()
        );

        return itemView;
    }
    static class RowBeanHolder
    {
        TextView txtTitle;
    }
}
