package com.example.celiko.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.celiko.loginandregistration.R;
import com.example.celiko.model.User;

import java.util.List;

/**
 * Created by celiko on 2015-07-13.
 */
public class UserAdapter extends ArrayAdapter<User> {
    Context context;
    int resource;
    List<User> user=null;

    public UserAdapter(Context context, int resource, List<User> user) {
        super(context, resource, user);
        this.context = context;
        this.resource = resource;
        this.user = user;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        RowBeanHolder holder = null;
        if(itemView== null){

            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            itemView = inflater.inflate(resource, parent, false);
            holder = new RowBeanHolder();
            holder.txtTitle=(TextView)itemView.findViewById(R.id.textView1);

            itemView.setTag(holder);
        }else {
            holder=(RowBeanHolder) itemView.getTag();
        }

        User u = user.get(position);
        holder.txtTitle.setText(u.getId()+" "+u.getExternalId()+" "+u.getFirstName()+" "+u.getLastName()+" "+u.getLogin()+" "+u.getCreatedAt());

        return itemView;
    }
    static class RowBeanHolder
    {
        TextView txtTitle;
    }

}
